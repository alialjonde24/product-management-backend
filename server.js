const http = require('http');
const express = require('express');
var cors = require('cors');
var mongoUtil = require('./database');
const productRouter = require('./routes/product');
const loginRouter = require('./routes/auth');

const app = express();
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb'}));
app.use(cors({ origin: 'http://localhost:4200' }));

var mongoUtil = require('./database');

mongoUtil.connectToServer(function (client) {
    app.use('/login', loginRouter);
    app.use('/product', productRouter);
    app.use('/', function (req, res) {
        res.send('Product Management');
    });

    const server = http.createServer(app);

    const port = 3000;

    app.locals.collection = client;

    server.listen(port);

    console.debug('Server listening on port ' + port);
});
 

 