const express = require('express');
var ObjectId = require('mongodb').ObjectID;
const router = express.Router();

router.get('/pageSize=:pageSize&pageNumber=:pageNumber', async function (req, res) {
    const collection = req.app.locals.collection.collection('Product');
    let count = 0
    await collection.stats().then(response => { count = response.count })
    await collection.find({}).sort({ "createDate": -1 }).skip(parseInt(req.params.pageNumber * req.params.pageSize)).limit(parseInt(req.params.pageSize)).toArray().then(response => res.status(200).json(
        {
            count: count,
            pages: Math.ceil(count / req.params.pageSize),
            data: response
        }
    ));
});

router.get('/:id', function (req, res) {
    const collection = req.app.locals.collection.collection('Product');
    collection.find(ObjectId(req.params.id)).toArray().then(response => res.status(200).json(response));
});

router.post('/', function (req, res) {
    const collection = req.app.locals.collection.collection('Product');
    let newItem = {
        title: req.body.title,
        description: req.body.description,
        image: req.body.image,
        price: req.body.price,
        quantity: req.body.quantity,
        createDate: new Date(),
    };
    collection.insertOne(newItem).then(response => res.status(200).json(response.ops));
});

router.put('/:id', function (req, res) {
    const collection = req.app.locals.collection.collection('Product');
    let updated = {
        title: req.body.title,
        description: req.body.description,
        image: req.body.image,
        price: req.body.price,
        quantity: req.body.quantity,
        createDate: new Date(),
    };
    collection.findOneAndUpdate(
        { "_id": ObjectId(req.params.id) },
        { $set: updated },
        { upsert: true }, function (err, result) {
            if (err) {
                res.send(err)
            }
            else {
                if (result.ok > 0) {
                    updated._id = req.params.id
                    res.status(200).json(updated)
                } else {
                    res.status(404).json({ "message": "update Failure" })
                }
            }
        })
});


router.delete('/:id', function (req, res) {
    const collection = req.app.locals.collection.collection('Product');
    collection.deleteOne({ "_id": ObjectId(req.params.id) }).then(response => {
        console.log(response)
        if (response.deletedCount > 0) {
            res.status(200).json({ "message": "deleted successfully" })
        } else {
            res.status(404).json({ "message": "delete Failure" })
        }
    });
});


module.exports = router;