const express = require('express');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
// create new router 
const router = express.Router();
const accessTokenSecret = '123';

router.post('/', async function (req, res) {
    const { username, password } = req.body;
    const collection = req.app.locals.collection.collection('auth');
    let user
    await collection.findOne({ "username": username }).then(response => user = response)
    if (user && user.password == password) {
        // Generate an access token
        const accessToken = jwt.sign({ username: user.username, role: user.role }, accessTokenSecret);
        res.json({accessToken});
    } else {
        res.send({message:'Username or Password Incorrect'});
    }
});

module.exports = router;
